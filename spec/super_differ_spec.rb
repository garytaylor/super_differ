# frozen_string_literal: true

RSpec.describe SuperDiffer do
  subject(:differ) { described_class }

  it 'has a version number' do
    expect(SuperDiffer::VERSION).not_to be_nil
  end

  describe '.diff_html' do
    it 'delegates to HtmlDiffer' do
      mock_differ = class_spy(SuperDiffer::HtmlDiffer).as_stubbed_const
      differ.diff_html('hElLo is that documize!', 'Hello is that Documize?')
      expect(mock_differ).to have_received(:diff).with('hElLo is that documize!', 'Hello is that Documize?')
    end
  end
end
