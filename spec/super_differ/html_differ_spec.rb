# frozen_string_literal: true

RSpec.describe SuperDiffer::HtmlDiffer do
  subject(:differ) { described_class }

  let(:expected_replaced_span) { 'background-color: lightskyblue; text-decoration: overline;' }
  let(:expected_inserted_span) { 'background-color: palegreen; text-decoration: underline;' }

  include_context 'html example data'

  describe '.diff' do
    it 'cleans tags' do
      expectation = 'chinese中文'
      expect(differ.diff('chinese中文', `chinese<documize type="field-start"></documize>中文`)).to include(expectation)
    end

    it 'highlights the removal of text before the chinese symbols' do
      expectation = '<span style="background-color: lightpink; text-decoration: line-through;">chinese</span>中文'
      expect(differ.diff('chinese中文', '中文')).to include(expectation)
    end

    it 'highlights the removal of the chinese symbols after the text' do
      expectation = 'chinese<span style="background-color: lightpink; text-decoration: line-through;">中文</span>'
      expect(differ.diff('chinese中文', 'chinese')).to include(expectation)
    end

    it 'compares basic text' do
      expectation = '<span style="background-color: lightpink; text-decoration: line-through;">hE</span><span style="background-color: palegreen; text-decoration: underline;">Hel</span>l<span style="background-color: lightpink; text-decoration: line-through;">L</span>o is that <span style="background-color: lightpink; text-decoration: line-through;">d</span><span style="background-color: palegreen; text-decoration: underline;">D</span>ocumize<span style="background-color: lightpink; text-decoration: line-through;">!</span><span style="background-color: palegreen; text-decoration: underline;">?</span>'
      expect(differ.diff('hElLo is that documize!', 'Hello is that Documize?')).to eq(expectation)
    end

    it 'highlights change to italic' do
      expectation = '<i><span style="background-color: lightskyblue; text-decoration: overline;">abc</span></i>'
      expect(differ.diff('abc', '<i>abc</i>')).to eql(expectation)
    end

    it 'highlights change to italic and wrapped in h1' do
      expectation = '<h1><i><span style="background-color: lightskyblue; text-decoration: overline;">abc</span></i></h1>'
      expect(differ.diff('abc', '<h1><i>abc</i></h1>')).to eql(expectation)
    end

    it 'highlights change from span within paragraph to normal text' do
      expectation = "<span style=\"#{expected_replaced_span}\">def</span>"
      expect(differ.diff('<p><span>def</span></p>', 'def')).to eql(expectation)
    end

    it 'highlights the removal of an image' do
      expectation = 'Documize Logo:<span style="background-color: lightpink; text-decoration: line-through;"><img src="http://documize.com/img/documize-logo.png" alt="Documize"/></span>'
      expect(differ.diff('Documize Logo:<img src="http://documize.com/img/documize-logo.png" alt="Documize">',
                         'Documize Logo:')).to eql(expectation)
    end

    it 'highlights the removal of text before an image' do
      expectation = '<span style="background-color: lightpink; text-decoration: line-through;">Documize Logo:</span><img src="http://documize.com/img/documize-logo.png" alt="Documize"/>'
      expect(differ.diff('Documize Logo:<img src="http://documize.com/img/documize-logo.png" alt="Documize">',
                         '<img src="http://documize.com/img/documize-logo.png" alt="Documize">')).to eql(expectation)
    end

    it 'highlights the replacement of li text' do
      expectation = '<ul><li><span style="background-color: lightpink; text-decoration: line-through;">1</span><span style="background-color: palegreen; text-decoration: underline;">one</span></li><li><span style="background-color: lightpink; text-decoration: line-through;">2</span><span style="background-color: palegreen; text-decoration: underline;">two</span></li><li><span style="background-color: lightpink; text-decoration: line-through;">3</span><span style="background-color: palegreen; text-decoration: underline;">three</span></li></ul>'
      expect(differ.diff('<ul><li>1</li><li>2</li><li>3</li></ul>',
                         '<ul><li>one</li><li>two</li><li>three</li></ul>')).to eql(expectation)
    end

    it 'highlights the addition on an li' do
      expectation = '<ul><li>1</li><li><i><span style="background-color: lightskyblue; text-decoration: overline;">2</span></i></li><li>3</li><li><span style="background-color: palegreen; text-decoration: underline;">4</span></li></ul>'
      expect(differ.diff('<ul><li>1</li><li>2</li><li>3</li></ul>',
                         '<ul><li>1</li><li><i>2</i></li><li>3</li><li>4</li></ul>')).to eql(expectation)
    end

    it 'highlights the removal of doc2' do
      expectation = '<li><span style="background-color: lightpink; text-decoration: line-through;">Automated document formatting</span></li>'
      expect(differ.diff(doc1 + doc2 + doc3 + doc4, doc1 + doc3 + doc4)).to include(expectation)
    end

    it 'highlights doc2 being made italic' do
      expectation = '<span style="background-color: lightskyblue; text-decoration: overline;">Automated document formatting</span>'
      expect(differ.diff(doc1 + doc2 + doc3 + doc4, "#{doc1}<i>#{doc2}</i>#{doc3}#{doc4}")).to include(expectation)
    end

    it 'highlights insertion after doc2' do
      expectation = '<span style="background-color: palegreen; text-decoration: underline;">inserted</span>'
      expect(differ.diff(doc1 + doc2 + doc3 + doc4, "#{doc1}#{doc2}inserted#{doc3}#{doc4}")).to include(expectation)
    end

    it 'highlights a new div added between doc3 and doc4' do
      expectation = '<span style="background-color: palegreen; text-decoration: underline;">New Div</span>'
      expect(differ.diff(doc1 + doc2 + doc3 + doc4,
                         "#{doc1}#{doc2}#{doc3}<div><p>New Div</p></div>#{doc4}")).to include expectation
    end

    it 'highlights a new div added between bbcNews1 and bbcNews2' do
      expectation = "<div><i><span style=\"#{expected_inserted_span}\">HTML-Diff-Inserted</span></i></div>"
      expect(differ.diff(bbc_news1 + bbc_news2,
                         "#{bbc_news1}<div><i>HTML-Diff-Inserted</i></div>#{bbc_news2}")).to include expectation
    end
  end
end
