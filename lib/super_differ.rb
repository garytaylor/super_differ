# frozen_string_literal: true

require 'super_differ/version'
require 'super_differ/html_differ'

# SuperDiffer is the top level module for the gem
# it has methods which delegates to the appropriate differ module
# depending on the type of diff required
module SuperDiffer
  class Error < StandardError; end

  def self.diff_html(*args, **kw_args)
    HtmlDiffer.diff(*args, **kw_args)
  end
end
