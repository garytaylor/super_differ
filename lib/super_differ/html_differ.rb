# frozen_string_literal: true

require 'ffi'
require 'os'

module SuperDiffer
  # A differ that diffs HTML
  # Currently based on the golang html-diff library but may well be replaced with ruby implementation
  module HtmlDiffer
    extend FFI::Library
    operating_system = if OS.mac?
                         :darwin
                       elsif OS.linux?
                         :linux
                       elsif OS.windows?
                         :windows
                       end
    ffi_lib File.absolute_path("../../precompiled/htmldiff-#{operating_system}-#{OS.host_cpu}.so", __dir__)

    attach_function :compare, %i[string string], :string

    def self.diff(str1, str2)
      mystr1 = (str1 || '')
      mystr2 = (str2 || '')
      result = compare(mystr1, mystr2)
      result.force_encoding 'utf-8'
    end
  end
end
