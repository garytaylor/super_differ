# frozen_string_literal: true

require_relative 'lib/super_differ/version'

Gem::Specification.new do |spec|
  spec.name          = 'super-differ'
  spec.version       = SuperDiffer::VERSION
  spec.authors       = ['Gary Taylor']
  spec.email         = ['gary@neudata.co']

  spec.summary       = 'A diffing library for variuous data formats'
  spec.description   = 'A diffing library for variuous data formats'
  spec.homepage      = 'http://unknown.com'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.7.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'http://unknown.com'
  spec.metadata['changelog_uri'] = 'http://unknown.com'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.extensions    = ['ext/super_differ/extconf.rb']
  spec.add_runtime_dependency 'ffi', '~> 1.15'
  spec.add_runtime_dependency 'os'
  spec.metadata['rubygems_mfa_required'] = 'true'
end
